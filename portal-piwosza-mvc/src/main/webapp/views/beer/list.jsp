<%@page import="pl.portalpiwosza.entity.Beer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Piwa</title>
</head>
<body>
	<h1>Piwa</h1>
	<c:url var="myurl" value="/beers/addBeer" /><a href="${myurl}">
	Dodaj piwo</a>
	<table border="1">
		<tr>
			<td>Lp.</td>
			<td>Nazwa</td>
			<td>Browar</td>
			<td>Styl</td>
			<td>Więcej</td>
			<td>Edycja</td>
			<td>Usunięcie</td>
		</tr>
		<%
			Long number = 1L;
		%>
		<c:forEach var="beer" items="${beers}">
			<tr>
				<td><%=number++%></td>
				<td><a href="beers/${beer.id}">${beer.name}</a></td>
				<td>
				<c:forEach var="brewery" items="${beer.breweries}">
				<c:url var="myurl" value="/breweries/${brewery.id}" /><a href="${myurl}">${brewery.name}</a>
				</c:forEach>
				</td>
				<td>${beer.style}</td>
				<td><a href="beers/${beer.id}">Więcej</a></td>
				<td><a href="beers/${beer.id}/edit" >Edycja</a></td>
				<td><a href="beers/${beer.id}/delete" method="post">Usunięcie</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="">Wróć na: Strona główna</a>
</body>
</html>