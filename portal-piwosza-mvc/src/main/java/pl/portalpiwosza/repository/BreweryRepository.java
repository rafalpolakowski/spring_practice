package pl.portalpiwosza.repository;

import org.springframework.data.repository.CrudRepository;

import pl.portalpiwosza.entity.Brewery;

public interface BreweryRepository extends CrudRepository<Brewery, Long> {

}
