package pl.portalpiwosza.dao;

import org.springframework.stereotype.Repository;

import pl.portalpiwosza.entity.Score;

@Repository
public class ScoreDao extends GenericDao<Score, Long>{

}
