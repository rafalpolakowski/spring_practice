<%@page import="pl.portalpiwosza.entity.Beer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dodawanie piwa</title>
</head>
<body>
	<h1>Dodawanie piwa</h1>
	<form action="addBeer" method="post">
		<input name="name" placeholder="Nazwa" /><br />
		<select name="brewery">
			<c:forEach items="${breweries}" var="brewery">
				<option value="${brewery.id}">${brewery.name}</option>
			</c:forEach>
		</select> <br />
		<select name="style">
			<c:forEach items="${styles}" var="style">
				<option value="${style.id}">${style.name}</option>
			</c:forEach>
		</select> <br />
		<input name="alcohol" placeholder="Alkohol" /><br /> 
		<input name="extract" placeholder="Ekstrakt" /><br />
		<textarea name="description">Opis piwa</textarea>
		<br /> <input type="submit" value="Dodaj" />
	</form>


</body>
</html>