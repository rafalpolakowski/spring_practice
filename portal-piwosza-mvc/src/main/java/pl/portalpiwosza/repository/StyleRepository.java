package pl.portalpiwosza.repository;

import org.springframework.data.repository.CrudRepository;

import pl.portalpiwosza.entity.Style;

public interface StyleRepository extends CrudRepository<Style, Long> {

	public Style findByName(String name);
}
