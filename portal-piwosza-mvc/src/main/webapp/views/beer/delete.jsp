<%@page import="pl.portalpiwosza.entity.Beer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Profil Piwa</title>
</head>
<body>
	<h1>Czy jesteś pewny że chcesz usunąć poniższe piwo?</h1>
	<table border="1">
		<tr>
			<td>Nazwa</td>
			<td>${beer.name}</td>
		</tr>
		<tr>
			<td>Browar</td><td>
			<c:forEach var="brewery" items="${beer.breweries}" >
			<c:url var="myurl" value="/breweries/${brewery.id}" /><a href="${myurl}">${brewery.name}</a>
			</c:forEach></td>
		</tr>
		<tr>
			<td>Styl</td>
			<td>${beer.style}</td>
		</tr>
		<tr>
			<td>Nazwa</td>
			<td>${beer.name}</td>
		</tr>
		<tr>
			<td>Alkohol</td>
			<td>${beer.alcohol}</td>
		</tr>
		<tr>
			<td>Ekstrakt</td>
			<td>${beer.extract}</td>
		</tr>
	</table>
	<form action="deleteBeerConfirmed" method="post">
	<input type="submit" value="Usuń piwo" />
	</form>
	<a href="<c:url value = "/beers"/>">Wróć na: Lista piw</a><br/>
	<a href="<c:url value = "/"/>">Wróć na: Stronę główną</a>
</body>
</html>