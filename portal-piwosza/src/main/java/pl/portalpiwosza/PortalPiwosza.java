package pl.portalpiwosza;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import pl.portalpiwosza.dao.BeerDao;
import pl.portalpiwosza.dao.BreweryRepository;
import pl.portalpiwosza.dao.NoteDao;
import pl.portalpiwosza.dao.UserDao;
import pl.portalpiwosza.entity.Beer;
import pl.portalpiwosza.entity.Brewery;
import pl.portalpiwosza.entity.Note;
import pl.portalpiwosza.entity.User;

@Configuration
@ComponentScan
public class PortalPiwosza {
	public static void main(String[] args) throws InterruptedException {

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(PortalPiwosza.class);

		//////////////////////////////////////////////////
		// Piwa //
		//////////////////////////////////////////////////

		System.out.println("Tworzymy piwa");
		Beer beer1 = new Beer("Pacyfic", 5.0, 12);
		Beer beer2 = new Beer("Atlantyk", 4.0, 10);
		Beer beer3 = new Beer("Indyk", 6.0, 14);

		System.out.println("Dodajemy piwa do bazy");
		BeerDao beerDao = ctx.getBean(BeerDao.class);
		beerDao.save(beer1);
		beerDao.save(beer2);
		beerDao.save(beer3);

		//////////////////////////////////////////////////
		// Browary //
		//////////////////////////////////////////////////

		System.out.println("Tworzymy browary");
		Brewery brewery1 = new Brewery("Pinta");
		Brewery brewery2 = new Brewery("Ale Browar");
		Brewery brewery3 = new Brewery("Artezan");

		System.out.println("Zapisujemy browary");
		BreweryRepository breweryDao = ctx.getBean(BreweryRepository.class);
		breweryDao.save(brewery1);
		breweryDao.save(brewery2);
		breweryDao.save(brewery3);

		//////////////////////////////////////////////////
		// Piwa+Browary //
		//////////////////////////////////////////////////

		System.out.println("Sprawdzamy czy obiekt ma id: " + beer1.getId());

		System.out.println("Dodajemy piwom browary");
		beer1.getBreweries().add(brewery1);
		beer2.getBreweries().add(brewery2);
		beer3.getBreweries().add(brewery1);
		System.out.println("Aktualizujemy piwa w bazie");
		beerDao.update(beer1);
		beerDao.update(beer2);
		beerDao.update(beer3);

//        System.out.println("Ustawiamy browarowi piwa");
//        List<Beer> beers = new ArrayList<>();
//        beers.add(beer1);
//        beers.add(beer3);
//        brewery1.setBeers(beers);  

		System.out.println("Object: " + brewery1);
		System.out.println("Piwka od browaru nr 1 : " + brewery1.getBeers());
		System.out.println("Browar dla piwa nr 1 : " + beer1.getBreweries());
		System.out.println("Database: " + breweryDao.get(1L));

		System.out.println("Object: " + brewery1);
		System.out.println("Database: " + breweryDao.get(1L));

		//////////////////////////////////////////////////
		// Sk�adniki //
		//////////////////////////////////////////////////

		beer1.getHop().add("Mosaic");
		beer1.getHop().add("Amarillo");
		beer1.getHop().add("Other");
//        beerDao.update(beer2);
//        beerDao.update(beer2);
		System.out.println("Tu nam si� apka wysypie");
		System.out.println("Piwo z chmielami - obiekt:" + beer1);
		System.out.println("Piwo z chmielami - baza:" + beerDao.get(1L));

		//////////////////////////////////////////////////
		// U�ytkownicy //
		//////////////////////////////////////////////////

		User user1 = new User("Rafa�", "rafal@op.pl", "Gdynia");
		User user2 = new User("S�awek", "slawek@op.pl", "Szemud");
		User user3 = new User("Sebastian", "sebastian@op.pl", "Gdynia");
		UserDao userDao = ctx.getBean(UserDao.class);
		userDao.save(user1);
		userDao.save(user2);
		userDao.save(user3);

		System.out.println(user1);
		System.out.println(user2);
		System.out.println(user3);

		user1.getKnown().add(beer1);
		user2.getFavorite().add(beer2);
		user3.getToTry().add(beer3);

//        userDao.update(user1);
//        userDao.update(user2);
//        userDao.update(user3);

		System.out.println(user1);
		System.out.println(user2);
		System.out.println(user3);

		//////////////////////////////////////////////////
		// Notatki //
		//////////////////////////////////////////////////

		Note note1 = new Note();
		Note note2 = new Note(user1, beer1, false, "Dobre piwo, �agodny smak, intensywny aromat");
		Note note3 = new Note(user2, beer2, true, "S�abe piwo i �mierdzi!");

		NoteDao noteDao = ctx.getBean(NoteDao.class);
		noteDao.save(note1);
		noteDao.save(note2);
		noteDao.save(note3);

		System.out.println(note1);
		System.out.println(note2);
		System.out.println(note3);

		note1.setAuthor(user3);
		System.out.println("Autor notatki nr 1: " + note1.getAuthor());
		note1.setBeer(beer3);
		note1.setPrivateNote(true);
		note1.setNote("Jako tako");

//        noteDao.update(note1);

		System.out.println(note1);
		System.out.println(user3);

		//////////////////////////////////////////////////
		ctx.close();
	}

}
