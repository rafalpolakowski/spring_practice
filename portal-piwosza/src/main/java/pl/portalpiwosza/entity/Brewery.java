package pl.portalpiwosza.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Brewery extends BasicEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String city;
	private String address;
	private String establishmentYear;
	private String logo;
	private String description;
	private String facebook;
	private String url;
	@ManyToMany(mappedBy="breweries", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	private Set<Beer> beers= new HashSet<>();
		
	
	public Brewery(String name) {
		super();
		this.name = name;
	}
	
	public Brewery() {} 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEstablishmentYear() {
		return establishmentYear;
	}
	public void setEstablishmentYear(String establishmentYear) {
		this.establishmentYear = establishmentYear;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}


	public Set<Beer> getBeers() {
		return beers;
	}

	public void setBeers(Set<Beer> beers) {
		this.beers = beers;
	}

	@Override
	public String toString() {
		return "Brewery [" + (this.getId() != null ? "id=" + this.getId() + ", " : "") + (name != null ? "name=" + name + ", " : "")
				+ (city != null ? "city=" + city + ", " : "") + (address != null ? "address=" + address + ", " : "")
				+ (establishmentYear != null ? "establishmentYear=" + establishmentYear + ", " : "")
				+ (logo != null ? "logo=" + logo + ", " : "")
				+ (description != null ? "description=" + description + ", " : "")
				+ (facebook != null ? "facebook=" + facebook + ", " : "") + (url != null ? "url=" + url + ", " : "")
				+ (beers != null ? "beers=" + beers.size() + ", " : "")
				+ "]";
	}

}
