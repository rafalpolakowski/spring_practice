package pl.portalpiwosza.repository;

import org.springframework.data.repository.CrudRepository;

import pl.portalpiwosza.entity.Score;

public interface ScoreRepository extends CrudRepository<Score, Long> {

}
