package pl.portalpiwosza.entity;

import java.io.Serializable;

import javax.persistence.Entity;



@Entity
public class Style extends BasicEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	
	public Style() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
