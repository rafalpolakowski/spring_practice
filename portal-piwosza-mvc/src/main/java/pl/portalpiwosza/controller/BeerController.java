package pl.portalpiwosza.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pl.portalpiwosza.entity.Beer;
import pl.portalpiwosza.entity.Brewery;
import pl.portalpiwosza.entity.Style;
import pl.portalpiwosza.repository.BeerRepository;
import pl.portalpiwosza.repository.BreweryRepository;
import pl.portalpiwosza.repository.StyleRepository;

@Controller
@RequestMapping("/beers")
public class BeerController {

	@Autowired
	public BeerRepository beerRepo;

	@Autowired
	public BreweryRepository breweryRepo;

	@Autowired
	public StyleRepository styleRepo;

	@RequestMapping("")
	public String list(Model model) {
		List<Beer> beers = (List<Beer>) beerRepo.findAll();
		model.addAttribute("beers", beers);
		return "beer/list";
	}

	@RequestMapping("/{id}")
	public String profile(@PathVariable Long id, Model model) {
		Beer beer = beerRepo.findById(id).get();
		model.addAttribute("beer", beer);
		return "beer/profile";
	}

	@GetMapping("/{id}/edit")
	public String edit(@PathVariable Long id, Model model) {
		Beer beer = beerRepo.findById(id).get();
		model.addAttribute("beer", beer);
		List<Brewery> breweries = (List<Brewery>) breweryRepo.findAll();
		model.addAttribute("breweries", breweries);
		List<Style> styles = (List<Style>) styleRepo.findAll();
		model.addAttribute("styles", styles);
		return "beer/edit";
	}
	
	@PostMapping("/{id}/edit")
	public String save(@PathVariable Long id, @RequestParam String name, @RequestParam Long brewery,
			@RequestParam Long style, @RequestParam double alcohol, @RequestParam double extract,
			@RequestParam String description) {
		Beer beer = beerRepo.findById(id).get();
		beer.setName(name);
		beer.setAlcohol(alcohol);
		beer.setExtract(extract);
		Set<Brewery> breweries = new HashSet<>();
		if (brewery != -1) {
			breweries.add(breweryRepo.findById(brewery).get());
			beer.setBreweries(breweries);
		}
		if (style != -1) {
			beer.setStyle(styleRepo.findById(style).get());
		}
		beer.setDescription(description);
		beer = beerRepo.save(beer);
		return "beer/edited";
	}
	
	@GetMapping("/{id}/delete")
	public String delete(@PathVariable Long id, Model model) {
		Beer beer = beerRepo.findById(id).get();
		model.addAttribute("beer", beer);
		return "beer/delete";
	}
	
	@PostMapping("/{id}/deleteBeerConfirmed")
	public String deleteConfirmed(@PathVariable Long id) {
		beerRepo.deleteById(id);
		return "beer/deleted";
	}
	
	@GetMapping("/addBeer")
	public String addForm(Model model) {
		List<Brewery> breweries = (List<Brewery>) breweryRepo.findAll();
		model.addAttribute("breweries", breweries);
		List<Style> styles = (List<Style>) styleRepo.findAll();
		model.addAttribute("styles", styles);
		return "beer/add";
	}
	
	@PostMapping("/addBeer")
	public String add(@RequestParam String name, @RequestParam Long brewery, @RequestParam Long style,
			@RequestParam double alcohol, @RequestParam double extract, @RequestParam String description) {
		Beer newBeer = new Beer(name, alcohol, extract);
		Set<Brewery> breweries = new HashSet<>();
		breweries.add(breweryRepo.findById(brewery).get());
		newBeer.setBreweries(breweries);
		newBeer.setStyle(styleRepo.findById(style).get());
		newBeer.setDescription(description);
		newBeer = beerRepo.save(newBeer);
		return "beer/added";
	}

}
