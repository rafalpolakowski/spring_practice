package pl.portalpiwosza.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pl.portalpiwosza.entity.Beer;
import pl.portalpiwosza.entity.Brewery;
import pl.portalpiwosza.entity.Style;
import pl.portalpiwosza.repository.BeerRepository;
import pl.portalpiwosza.repository.BreweryRepository;
import pl.portalpiwosza.repository.StyleRepository;

@Controller
public class HomeController {

	@Autowired
	public BeerRepository beerRepo;

	@Autowired
	public BreweryRepository breweryRepo;

	@Autowired
	public StyleRepository styleRepo;

	@RequestMapping("/")
	public String home() {
		return "index";
	}

	@GetMapping("/breweries")
	public String breweries(Model model) {
		List<Brewery> breweries = (List<Brewery>) breweryRepo.findAll();
		model.addAttribute("breweries", breweries);
		return "brewery/list";
	}

	@RequestMapping("/breweries/{id}")
	public String brewery(@PathVariable Long id, Model model) {
		Brewery brewery = breweryRepo.findById(id).get();
		model.addAttribute("brewery", brewery);
		return "brewery/profile";
	}

	@GetMapping("/users")
	public String users() {
		return "user/list";
	}

}
