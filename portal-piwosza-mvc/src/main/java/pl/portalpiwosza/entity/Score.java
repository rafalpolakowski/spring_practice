package pl.portalpiwosza.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Score implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "score_id")
	private Long id;
	@ManyToOne
	@JoinColumn(name = "id_author")
	private User author; 
	@ManyToOne
	@JoinColumn(name = "id_beer")
	private Beer beer;
	private Date date;
	private boolean privateScore;
	private int score;
	
	
	public Score() {
		super();
	}


	public Score(User author, Beer beer, Date date, boolean privateScore, int score) {
		super();
		this.author = author;
		this.beer = beer;
		this.date = date;
		this.privateScore = privateScore;
		this.score = score;
	}


	public User getAuthor() {
		return author;
	}


	public void setAuthor(User author) {
		this.author = author;
	}


	public Beer getBeer() {
		return beer;
	}


	public void setBeer(Beer beer) {
		this.beer = beer;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public boolean isPrivateScore() {
		return privateScore;
	}


	public void setPrivateScore(boolean privateScore) {
		this.privateScore = privateScore;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "Score [" + (id != null ? "id=" + id + ", " : "") + (author != null ? "author=" + author + ", " : "")
				+ (beer != null ? "beer=" + beer + ", " : "") + (date != null ? "date=" + date + ", " : "")
				+ "privateScore=" + privateScore + ", score=" + score + "]";
	}
	
	
	
}
