package pl.portalpiwosza.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class User extends BasicEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String email;
	private String city;
	@ManyToMany
    @JoinTable(name = "user_beer_to_try",
    joinColumns = {@JoinColumn(name="id_user", referencedColumnName="id")},
    inverseJoinColumns = {@JoinColumn(name="id_beer", referencedColumnName="id")})
	private Set<Beer> toTry = new HashSet<>();
	@ManyToMany
    @JoinTable(name = "user_favorite_beer",
    joinColumns = {@JoinColumn(name="id_user", referencedColumnName="id")},
    inverseJoinColumns = {@JoinColumn(name="id_beer", referencedColumnName="id")})
	private Set<Beer> favorite = new HashSet<>();
	@ManyToMany
    @JoinTable(name = "user_known_beer",
    joinColumns = {@JoinColumn(name="id_user", referencedColumnName="id")},
    inverseJoinColumns = {@JoinColumn(name="id_beer", referencedColumnName="id")})
	private Set<Beer> known = new HashSet<>();
	@OneToMany(mappedBy="author") 
	private Set<Note> notes = new HashSet<>();
	@OneToMany(mappedBy="author") 
	private Set<Score> scores = new HashSet<>();
	
	
	public User() {
		super();
	}


	public User(String name, String email, String city) {
		super();
		this.name = name;
		this.email = email;
		this.city = city;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Set<Beer> getToTry() {
		return toTry;
	}


	public void setToTry(Set<Beer> toTry) {
		this.toTry = toTry;
	}


	public Set<Beer> getFavorite() {
		return favorite;
	}


	public void setFavorite(Set<Beer> favorite) {
		this.favorite = favorite;
	}


	public Set<Beer> getKnown() {
		return known;
	}


	public void setKnown(Set<Beer> known) {
		this.known = known;
	}


	public Set<Note> getNotes() {
		return notes;
	}


	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}


	public Set<Score> getScores() {
		return scores;
	}


	public void setScores(Set<Score> scores) {
		this.scores = scores;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "User [" + (this.getId() != null ? "id=" + this.getId() + ", " : "") + (name != null ? "name=" + name + ", " : "")
				+ (email != null ? "email=" + email + ", " : "") + (city != null ? "city=" + city + ", " : "")
				+ (toTry != null ? "beers to Try =" + toTry.size() + ", " : "")
				+ (favorite != null ? "favorite beers =" + favorite.size() + ", " : "")
				+ (known != null ? "known beers =" + known.size() + ", " : "") + (notes != null ? "notes=" + notes.size() + ", " : "")
				+ (scores != null ? "scores=" + scores.size() : "") + "]";
	}
	
	
	
}
