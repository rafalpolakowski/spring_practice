package pl.portalpiwosza.dao;

import org.springframework.stereotype.Repository;

import pl.portalpiwosza.entity.Brewery;

@Repository
public class BreweryRepository extends GenericDao<Brewery, Long> {

}
