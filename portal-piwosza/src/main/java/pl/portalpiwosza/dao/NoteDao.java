package pl.portalpiwosza.dao;

import org.springframework.stereotype.Repository;

import pl.portalpiwosza.entity.Note;

@Repository
public class NoteDao extends GenericDao<Note, Long> {
	
}
