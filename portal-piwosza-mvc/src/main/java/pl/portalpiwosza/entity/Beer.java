package pl.portalpiwosza.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Beer extends BasicEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_style", referencedColumnName = "id")
	private Style style;
	private String description;
	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	private Set<Brewery> breweries = new HashSet<>();
	private double alcohol;
	private double extract;
	private boolean pasteurized;
	private boolean filtered;
	private String biterness;
	private String color;
	private String sweetness;
	@ElementCollection (fetch = FetchType.EAGER)
	private Set<String> hop = new HashSet<>();
	@ElementCollection (fetch = FetchType.EAGER)
	private Set<String> malt = new HashSet<>();
	@ElementCollection (fetch = FetchType.EAGER)
	private Set<String> yeast = new HashSet<>();
	private String production;
	private boolean authorization;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_beer", referencedColumnName = "id")
	@Fetch(FetchMode.SELECT)
	private List<Note> publicNotes = new ArrayList<>();
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_client", referencedColumnName = "id")
	@Fetch(FetchMode.SELECT)
	private List<Score> publicScores = new ArrayList<>();
	private double avgScore;

	public Beer() {
		super();
	}

	public Beer(String name, double alcohol, double extract) {
		super();
		this.name = name;
		this.alcohol = alcohol;
		this.extract = extract;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public double getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(double alcohol) {
		this.alcohol = alcohol;
	}

	public double getExtract() {
		return extract;
	}

	public void setExtract(double extract) {
		this.extract = extract;
	}

	public boolean isPasteurized() {
		return pasteurized;
	}

	public void setPasteurized(boolean pasteurized) {
		this.pasteurized = pasteurized;
	}

	public boolean isFiltered() {
		return filtered;
	}

	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}

	public String getBiterness() {
		return biterness;
	}

	public void setBiterness(String biterness) {
		this.biterness = biterness;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSweetness() {
		return sweetness;
	}

	public void setSweetness(String sweetness) {
		this.sweetness = sweetness;
	}


	public String getProduction() {
		return production;
	}

	public void setProduction(String production) {
		this.production = production;
	}

	public boolean isAuthorization() {
		return authorization;
	}

	public void setAuthorization(boolean authorization) {
		this.authorization = authorization;
	}

	public List<Note> getPublicNotes() {
		return publicNotes;
	}

	public void setPublicNotes(List<Note> publicNotes) {
		this.publicNotes = publicNotes;
	}

	public List<Score> getPublicScores() {
		return publicScores;
	}

	public void setPublicScores(List<Score> publicScores) {
		this.publicScores = publicScores;
	}

	public double getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(double avgScore) {
		this.avgScore = avgScore;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Brewery> getBreweries() {
		return breweries;
	}

	public void setBreweries(Set<Brewery> breweries) {
		this.breweries = breweries;
	}

	public Set<String> getHop() {
		return hop;
	}

	public void setHop(Set<String> hop) {
		this.hop = hop;
	}

	public Set<String> getMalt() {
		return malt;
	}

	public void setMalt(Set<String> malt) {
		this.malt = malt;
	}

	public Set<String> getYeast() {
		return yeast;
	}

	public void setYeast(Set<String> yeast) {
		this.yeast = yeast;
	}

	@Override
	public String toString() {
		return "Beer [" + (name != null ? "name=" + name + ", " : "") + (style != null ? "style=" + style + ", " : "")
				+ (description != null ? "description=" + description + ", " : "")
				+ (breweries != null ? "breweries=" + breweries + ", " : "") + "alcohol=" + alcohol + ", extract="
				+ extract + ", pasteurized=" + pasteurized + ", filtered=" + filtered + ", "
				+ (biterness != null ? "biterness=" + biterness + ", " : "")
				+ (color != null ? "color=" + color + ", " : "")
				+ (sweetness != null ? "sweetness=" + sweetness + ", " : "") + (hop != null ? "hop=" + hop + ", " : "")
				+ (malt != null ? "malt=" + malt + ", " : "") + (yeast != null ? "yeast=" + yeast + ", " : "")
				+ (production != null ? "production=" + production + ", " : "") + "authorization=" + authorization
				+ ", " + (publicNotes != null ? "publicNotes=" + publicNotes.size() + ", " : "")
				+ (publicScores != null ? "publicScores=" + publicScores.size() + ", " : "") + "avgScore=" + avgScore + "]";
	}
	
}