package pl.portalpiwosza.repository;

import org.springframework.data.repository.CrudRepository;

import pl.portalpiwosza.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
