<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Browary</title>
</head>
<body>
	<h1>Browary</h1>

	<table border="1">
		<tr>
			<td>Lp.</td>
			<td>Nazwa</td>
			<td>Miasto</td>
			<td>Rok założenia</td>
			<td>Więcej</td>
		</tr>
		<%
			Long number = 1L;
		%>
		<c:forEach var="brewery" items="${breweries}">
			<tr>
				<td><%=number++%></td>
				<td>${brewery.name}</td>
				<td>${brewery.city}</td>
				<td>${brewery.establishmentYear}</td>
				<td><c:url var="myurl" value="/breweries/${brewery.id}" /><a href="${myurl}">Więcej</a></td>
			</tr>
		</c:forEach>
	</table>

	<a href="">Wróć na: Strona główna</a>
</body>
</html>