package pl.portalpiwosza.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
//import javax.transaction.Transactional;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class GenericDao<T, K> {

	@PersistenceContext
	private EntityManager entityManager;
	private Class<T> type;

	@SuppressWarnings("unchecked")
	GenericDao() {
		type = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public T get(K key) {
		return entityManager.find(type, key);
	}

	public void save(T entity) {
		entityManager.persist(entity);
		System.out.println("Entity of type " + type + " saved.");
	}

	public void update(T entity) {
		entityManager.merge(entity);
		System.out.println("Entity of type " + type + " will be updated.");
	}

	public void remove(K key) {
		entityManager.remove(key);
		System.out.println("Entity of type " + type + " removed.");

	}

	public void refresh(T entity) {
		entityManager.refresh(this.entityManager.merge(entity));
		System.out.println("Entity of type " + type + " refreshed.");

	}
}
