package pl.portalpiwosza.dao;

import org.springframework.stereotype.Repository;

import pl.portalpiwosza.entity.User;

@Repository
public class UserDao extends GenericDao<User, Long> {
	
}
