package pl.portalpiwosza.repository;

import org.springframework.data.repository.CrudRepository;

import pl.portalpiwosza.entity.Note;

public interface NoteRepository extends CrudRepository<Note, Long> {

}
