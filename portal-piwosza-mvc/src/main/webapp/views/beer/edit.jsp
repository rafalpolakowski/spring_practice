<%@page import="pl.portalpiwosza.entity.Beer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edycja profilu piwa</title>
</head>
<body>
	<h1>Edycja profilu piwa</h1>
	<form method="post">
		<input name="name" value="${beer.name}" /><br /> <select
			name="brewery">
			<c:if test="${beer.breweries.isEmpty()}">
				<option value="-1" label="Wybierz browar" />
			</c:if>
			<c:if test="${!beer.breweries.isEmpty()}">
				<c:forEach items="${beer.breweries}" var="brewery">
					<option value="${brewery.id}">${brewery.name}</option>>
				</c:forEach>
			</c:if>
			<c:forEach items="${breweries}" var="brewery">
				<option value="${brewery.id}">${brewery.name}</option>
			</c:forEach>
		</select> <br /> <select name="style">
			<c:if test="${beer.style==null}">
				<option value="-1" label="Wybierz styl" />
			</c:if>
			<c:if test="${beer.style!=null}">
					<option value="${beer.style.id}">${beer.style.name}</option>>
			</c:if>
			<c:forEach items="${styles}" var="style">
				<option value="${style.id}">${style.name}</option>
			</c:forEach>
		</select> <br /> <input name="alcohol" value="${beer.alcohol}" /><br /> <input
			name="extract" value="${beer.extract}" /><br />
		<textarea name="description">${beer.description}</textarea>
		<br /> <input type="submit" value="Zapisz zmiany" />
	</form>
</body>
</html>