package pl.portalpiwosza;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import pl.portalpiwosza.entity.Beer;
import pl.portalpiwosza.entity.Brewery;
import pl.portalpiwosza.entity.Note;
import pl.portalpiwosza.entity.User;
import pl.portalpiwosza.repository.BeerRepository;
import pl.portalpiwosza.repository.BreweryRepository;
import pl.portalpiwosza.repository.NoteRepository;
import pl.portalpiwosza.repository.UserRepository;

@Configuration
@ComponentScan
public class PortalPiwosza {
	public static void main(String[] args) throws InterruptedException {

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(PortalPiwosza.class);

		//////////////////////////////////////////////////
		// Piwa //
		//////////////////////////////////////////////////

		System.out.println("Tworzymy piwa");
		Beer beer1 = new Beer("Pacyfic", "Pale Ale", 5.0, 12);
		Beer beer2 = new Beer("Atlantyk", "Pale Ale", 4.0, 10);
		Beer beer3 = new Beer("Indyk", "India Pale Ale", 6.0, 14);

		System.out.println("Dodajemy piwa do bazy");
		BeerRepository beerRepo = ctx.getBean(BeerRepository.class);
		beerRepo.save(beer1);
		beerRepo.save(beer2);
		beerRepo.save(beer3);

		//////////////////////////////////////////////////
		// Browary //
		//////////////////////////////////////////////////

		System.out.println("Tworzymy browary");
		Brewery brewery1 = new Brewery("Pinta");
		Brewery brewery2 = new Brewery("Ale Browar");
		Brewery brewery3 = new Brewery("Artezan");

		System.out.println("Zapisujemy browary");
		BreweryRepository breweryRepo = ctx.getBean(BreweryRepository.class);
		breweryRepo.save(brewery1);
		breweryRepo.save(brewery2);
		breweryRepo.save(brewery3);

		//////////////////////////////////////////////////
		// Piwa+Browary //
		//////////////////////////////////////////////////

		System.out.println("Sprawdzamy czy obiekt ma id: " + beer1.getId());

		System.out.println("Dodajemy piwom browary");
		beer1.getBreweries().add(brewery1);
		beer2.getBreweries().add(brewery2);
		beer3.getBreweries().add(brewery1);
		System.out.println("Aktualizujemy piwa w bazie");
		beer1 = beerRepo.save(beer1);
		beer2 = beerRepo.save(beer2);
		beer3 = beerRepo.save(beer3);

//        System.out.println("Ustawiamy browarowi piwa");
//        List<Beer> beers = new ArrayList<>();
//        beers.add(beer1);
//        beers.add(beer3);
//        brewery1.setBeers(beers);  

		System.out.println("Object: " + brewery1);
		System.out.println("Piwka od browaru nr 1 : " + brewery1.getBeers());
		System.out.println("Browar dla piwa nr 1 : " + beer1.getBreweries());
		System.out.println("Database: " + breweryRepo.findById(1L));

		System.out.println("Object: " + brewery1);
		System.out.println("Database: " + breweryRepo.findById(1L));

		//////////////////////////////////////////////////
		// Sk�adniki //
		//////////////////////////////////////////////////

		beer1.getHop().add("Mosaic");
		beer1.getHop().add("Amarillo");
		beer1.getHop().add("Other");
		System.out.println("Do tego miejsca dzia�a");
		beer1 = beerRepo.save(beer1);
		System.out.println("Tu nam si� apka wysypie");
		System.out.println("Piwo z chmielami - obiekt:" + beer1);
		System.out.println("Piwo z chmielami - baza:" + beerRepo.findById(1L));

		//////////////////////////////////////////////////
		// U�ytkownicy //
		//////////////////////////////////////////////////

		User user1 = new User("Rafa�", "rafal@op.pl", "Gdynia");
		User user2 = new User("S�awek", "slawek@op.pl", "Szemud");
		User user3 = new User("Sebastian", "sebastian@op.pl", "Gdynia");
		UserRepository userRepo = ctx.getBean(UserRepository.class);
		userRepo.save(user1);
		userRepo.save(user2);
		userRepo.save(user3);

		System.out.println(user1);
		System.out.println(user2);
		System.out.println(user3);

		user1.getKnown().add(beer1);
		user2.getFavorite().add(beer2);
		user3.getToTry().add(beer3);

		userRepo.save(user1);
		userRepo.save(user2);
		userRepo.save(user3);

		System.out.println(user1);
		System.out.println(user2);
		System.out.println(user3);

		//////////////////////////////////////////////////
		// Notatki //
		//////////////////////////////////////////////////

		Note note1 = new Note();
		Note note2 = new Note(user1, beer1, false, "Dobre piwo, �agodny smak, intensywny aromat");
		Note note3 = new Note(user2, beer2, true, "S�abe piwo i �mierdzi!");

		NoteRepository noteRepo = ctx.getBean(NoteRepository.class);
		noteRepo.save(note1);
		noteRepo.save(note2);
		noteRepo.save(note3);

		System.out.println(note1);
		System.out.println(note2);
		System.out.println(note3);

		note1.setAuthor(user3);
		System.out.println("Autor notatki nr 1: " + note1.getAuthor());
		note1.setBeer(beer3);
		note1.setPrivateNote(true);
		note1.setNote("Jako tako");

		noteRepo.save(note1);

		System.out.println(note1);
		System.out.println(user3);

		//////////////////////////////////////////////////
		ctx.close();
	}

}
