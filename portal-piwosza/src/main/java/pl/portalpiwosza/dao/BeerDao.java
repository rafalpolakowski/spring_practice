package pl.portalpiwosza.dao;

import org.springframework.stereotype.Repository;

import pl.portalpiwosza.entity.Beer;

@Repository
public class BeerDao extends GenericDao<Beer, Long> {

}
