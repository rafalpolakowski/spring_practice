package pl.portalpiwosza.repository;

import org.springframework.data.repository.CrudRepository;

import pl.portalpiwosza.entity.Beer;

public interface BeerRepository extends CrudRepository<Beer, Long> {

}
