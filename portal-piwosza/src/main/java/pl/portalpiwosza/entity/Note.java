package pl.portalpiwosza.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Note extends BasicEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User author;
	@ManyToOne
	@JoinColumn(name = "beer_id")
	private Beer beer;
	private LocalDate date;
	private boolean privateNote; 
	private String note;
	
		
	public Note() {
		super();
	}

	public Note(User author, Beer beer, boolean privateNote, String note) {
		super();
		this.author = author;
		this.beer = beer;
		this.date = LocalDate.now();
		this.privateNote = privateNote;
		this.note = note;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Beer getBeer() {
		return beer;
	}

	public void setBeer(Beer beer) {
		this.beer = beer;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public boolean isPrivateNote() {
		return privateNote;
	}

	public void setPrivateNote(boolean privateNote) {
		this.privateNote = privateNote;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Note [" + (this.getId() != null ? "id=" + this.getId() + ", " : "") + (author != null ? "author=" + author.getName() + ", " : "")
				+ (beer != null ? "beer=" + beer.getName() + ", from brewery " + beer.getBreweries().iterator().next().getName() + ", " : "") + (date != null ? "date=" + date + ", " : "")
				+ "privateNote=" + privateNote + ", " + (note != null ? "note=" + note : "") + "]";
	} 
	
	
	
}
