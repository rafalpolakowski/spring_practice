<%@page import="pl.portalpiwosza.entity.Beer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Profil Browaru</title>
</head>
<body>
	<h1></h1>
	<table border="1">
		<tr>
			<td>Nazwa</td>
			<td>${brewery.name}</td>
		</tr>
		<tr>
			<td>Miasto</td>
			<td>${brewery.city}</td>
		</tr>
		<tr>
			<td>Rok założenia</td>
			<td>${brewery.establishmentYear}</td>
		</tr>
		<tr>
			<td>Adres</td>
			<td>${brewery.address}</td>
		</tr>
		<tr>
			<td>Piwa</td>
			<td><c:forEach var="beer" items="${brewery.beers}">
				<c:url var="myurl" value="/beers/${beer.id}" /><a href="${myurl}">${beer.name}</a><br>
				</c:forEach></td>
		</tr>
	</table>
	<a href="<c:url value = "/breweries"/>">Wróć na: Lista browarów</a>
	<br />
	<a href="<c:url value = "/"/>">Wróć na: Stronę główną</a>
</body>
</html>